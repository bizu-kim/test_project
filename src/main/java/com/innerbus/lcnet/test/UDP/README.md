# UDP 수집 테스트

## UDP 수집 방식 2가지

java udp datagramchannel 이용한 코드

com/innerbus/lcnet/test/UDP/PureMain.java  
`jre/bin/java -XX:+UseParallelGC -XX:ParallelGCThreads=8 -XX:NewRatio=1 -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.Log4j2LogDelegateFactory -Xms4g -Xmx4g -cp lib/UDP-1.0-SNAPSHOT-fat.jar com.innerbus.lcnet.test.UDP.PureMain`

vertx udp datagramsocket 이용한 코드

com/innerbus/lcnet/test/UDP/Main.java  
`jre/bin/java -XX:+UseParallelGC -XX:ParallelGCThreads=8 -XX:NewRatio=1 -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.Log4j2LogDelegateFactory -Xms4g -Xmx4g -jar lib/UDP-1.0-SNAPSHOT-fat.jar`


## 실행방법

1. 프로젝트 빌드 후 `LogCenter/lib` 디렉토리에 `fat.jar` 파일을 위치시킨다.

2. 위의 수집방식중 한가지를 선택해서 UDP 서버를 생성한다.
   
3. `tool` 디렉토리의 `stress_gen`을 이용하여 UDP패킷을 생성한다.


## stress_gen 사용방법

stress_gen 이용해서 대상 서버에 UDP패킷 생성

> ./stress_gen {source_port} {source_address} {dest_port} {dest_address} {EPS} {Message} {rotate y/n} {ip bandwidth}

ex) 192.168.100.40 서버의 678포트에 4000EPS로 UDP 패킷 생성  
`stress_gen 700 192.168.100.10 678 192.168.100.40 4000 stress_test.log y 1`


