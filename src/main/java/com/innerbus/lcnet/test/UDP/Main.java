package com.innerbus.lcnet.test.UDP;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.datagram.DatagramSocketOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.WorkerExecutor;
import io.vertx.rxjava.core.datagram.DatagramPacket;
import io.vertx.rxjava.core.datagram.DatagramSocket;
import io.vertx.rxjava.core.shareddata.LocalMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rx.functions.Action1;
import rx.functions.Func2;

public class Main extends AbstractVerticle {
    public static void main(String[] args) {
        System.out.println("Start with vertx...");
    }

    private static final Logger logger = LogManager.getLogger(Main.class);
    private Integer port;
    private WorkerExecutor executor;
    private String ip;
    private LocalMap<String, JsonObject> authMapHolder, hostIdMap;
    //    private KafkaClient kafkaClient;
    private DatagramSocket udpServer;
    //    private UdpListener listner;
    public static Long timerId, packetCount = 0L, logCount = 0L, sendingCount = 0L;

//    private Func2<String, String, String> formattingData = (dateTime, data) -> {
//        final Buffer buffer = Buffer.buffer(dateTime).appendString(" ").appendString(data);
//        if (!data.endsWith("\n")) {
//            buffer.appendString("\n");
//        }
//        return buffer.toString();
//    };

//    private Function<DatagramPacket, Handler<Future<Object>>> datagramHandlerInBlocking = datagram -> future -> {
//        final String hostIp = datagram.sender().host();
//        final JsonObject dataHost = authMapHolder.get(hostIp);
//        if (dataHost != null) {
//            final String dateTime = ZonedDateTime.now().toLocalDateTime().format(DateTimeFormatter.ofPattern(DEF_FORMAT_YYYYMMDDHHMMSS.get()));
//            final KafkaClient kafkaClient = getKafkaClient.call(this.vertx);
//            final String date = dateTime.substring(0, 8);
//            final String fileName = supplyFileName.call(date);
//            final String hostId = dataHost.getString(SHAREDDATA_KEY_HOSTID);
//            final JsonObject hostInfo = getHostIdMapFromSharedData.call(this.vertx).get(hostId);
//            final String data = datagram.getDelegate().data().toString(hostInfo.getString(SHAREDDATA_KEY_ENCODE));
//            final JsonObject parsingMsg = makeParsingMsg.call(hostInfo, fileName, true, dateTime, formattingData.call(dateTime, data));
//            kafkaClient.runInThread(future, parsingMsg);
//        } else {
//            logger.debug("Datagram from : {} abandoned. (Not In Entry).", hostIp);
//            future.complete();
//        }
//    };

    public void start() {
        System.out.println("============== udp test start ==============");
        this.executor = this.vertx.createSharedWorkerExecutor("test", Runtime.getRuntime().availableProcessors() * 2);
        this.ip = "0.0.0.0";
//        this.ip = this.config().getString(DEF_BINDIP.get());
        this.port = 678;
//        this.authMapHolder = getAuthMapFromSharedData
//                .call(this.vertx, PROTOCOL_UDP.getCode(), String.valueOf(this.port));
//        this.kafkaClient = getKafkaClient.call(this.vertx);
//        this.hostIdMap = getHostIdMapFromSharedData.call(this.vertx);

        DatagramSocketOptions opts = new DatagramSocketOptions()
                .setBroadcast(false).setReuseAddress(true).setReusePort(true);

//        Thread thread = new Thread(() -> {
//            logger.debug("UdpListener Thread ...");
//            try {
//                this.listner = new UdpListener(port, 4096, 1024*1024, 10000,
//                        new DatagramSendToThread(authMapHolder, kafkaClient, hostIdMap));
//                this.listner.UdpServer_Run();
//            } catch (Exception e) {
//                Utils.getPrintStackTraceToString(e);
//            }
//        });
//        thread.start();

        this.udpServer = this.vertx.createDatagramSocket(opts);
        this.udpServer.rxListen(this.port, this.ip).subscribe(
                new Action1<DatagramSocket>() {
                    @Override
                    public void call(DatagramSocket socket) {
                        socket.toObservable().
                                subscribe(new Action1<DatagramPacket>() {
                                    @Override
                                    public void call(DatagramPacket datagramPacket) {
                                        logCount++;
//                            this.executor.rxExecuteBlocking(datagramHandlerInBlocking.apply(datagramPacket))
//                                    .subscribe(complete -> sendingCount++,
//                                            error -> logger.error("Datagram Sending Error : {}", error.getMessage()));
                                    }
                                });
                    }
                });

        System.out.printf("UDP Server [ %d : %s ] Deployed With RecvBufferSize:%d Clustered:%b %n", this.port, this.deploymentID()
                , opts.getReceiveBufferSize(), this.vertx.isClustered());

        Integer interval = this.config().getInteger("counting.log.interval", 10000);
        if (interval != 0) {
            timerId = this.vertx.setPeriodic(interval, new Handler<Long>() {
                @Override
                public void handle(Long id) {
                    packetCount += logCount;
                    System.out.printf("UDP Server Received count : %d, Interval count : %d, Send to Kafka : %d %n",
                            packetCount, logCount, sendingCount);
                    logCount = 0L;
                }
            });
        }
    }

    public void stop() {
        if (timerId != null) this.vertx.cancelTimer(timerId);
//        this.listner.Stop_UdpServer();
        this.udpServer.close(new Handler<AsyncResult<Void>>() {
            @Override
            public void handle(AsyncResult<Void> _void) {
                System.out.printf("UdpServer Closed : [ %d : %s ]\n", port, Main.this.deploymentID());
            }
        });
    }
}
