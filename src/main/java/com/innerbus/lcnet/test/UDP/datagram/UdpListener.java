package com.innerbus.lcnet.test.UDP.datagram;


import com.innerbus.lcnet.common.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;

public class UdpListener {
    private static final Logger logger = LogManager.getLogger(UdpListener.class);

    private int Bind_Port = 0;
    private int UdpMsg_Size = 0;
    private int TimeOut = 0;
    private DatagramChannel UdpChannel = null;
    private InetSocketAddress ServAddr = null;
    private Selector selector = null;
    private DatagramIOHandler fWriterHandle = null;

    public UdpListener(int port, int msgSize, int recvBufSize, int timeOut, DatagramIOHandler fWriterCallBack) {
        try {
            this.Bind_Port = port;
            this.UdpMsg_Size = msgSize;
            this.TimeOut = timeOut;
            this.ServAddr = new InetSocketAddress(this.Bind_Port);
            this.UdpChannel = DatagramChannel.open();
            this.UdpChannel.configureBlocking(false);
            this.UdpChannel.bind(this.ServAddr);
            this.UdpChannel.setOption(StandardSocketOptions.SO_RCVBUF, recvBufSize);
            this.UdpChannel.setOption(StandardSocketOptions.SO_REUSEADDR, true);
            this.selector = Selector.open();
            this.UdpChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(this.UdpMsg_Size));
            this.fWriterHandle = fWriterCallBack;
            System.out.println("UDP Listener Create At [ " + this.ServAddr.getHostString() + "/" + this.Bind_Port + " ]");
        } catch (IOException e) {
            System.out.println("Cannot initializ UDP Server");
            System.out.println(Utils.getPrintStackTraceToString(e));
        }
    }


    public void Stop_UdpServer() {
        try {
            this.UdpChannel.disconnect();
            logger.info("Udp Socket Closing Succeed");
        } catch (Exception e) {
            logger.error("Udp Socket Closing Failed : [ " + e.getMessage() + " ]");
        }

        try {
            this.UdpChannel.close();
            this.selector.wakeup();
            logger.info("Udp Channel Closing Succeed");
        } catch (Exception e) {
            logger.error("Udp Channel Closing Failed : [ " + e.getMessage() + " ]");
        }
    }

    public void UdpServer_Run() {
        try {
            while (this.UdpChannel.isOpen()) {
                try {
                    if (selector.select(this.TimeOut) == 0) {
                        System.out.println("No Datagram From UDP Client Within [ " + this.TimeOut + " msec ]");
                        continue;
                    }

                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

                    while (keyIterator.hasNext()) {
                        SelectionKey key = keyIterator.next();
                        keyIterator.remove();

                        if (key.isValid() && (key.interestOps() == SelectionKey.OP_READ)){
                            this.fWriterHandle.eventReadMsg(key);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("UDP Server Closed : [ " + e.getMessage() + " ]");
                    System.out.println(Utils.getPrintStackTraceToString(e));
                    break;
                }
            }
        } finally {
            System.out.println("UDP Server [ " + this.ServAddr.getHostString() + "/" + this.Bind_Port + " ] Terminated");

            try {
                if (this.UdpChannel != null) {
                    this.UdpChannel.disconnect();
                }
                this.UdpChannel = null;
            } catch (Exception e) {
            }

            if (this.ServAddr != null)
                this.ServAddr = null;
        }
    }
}

