package com.innerbus.lcnet.test.UDP.datagram;


import com.innerbus.lcnet.common.util.Utils;
import com.innerbus.lcnet.test.UDP.PureMain;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.shareddata.LocalMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;

public class DatagramSendToThread implements DatagramIOHandler {
    private static final Logger logger = LogManager.getLogger(DatagramSendToThread.class);

    private final LocalMap<String, JsonObject> authMapHolder, hostIdMapHolder;
//    private final KafkaClient kafkaClientHolder;

    public DatagramSendToThread(LocalMap<String, JsonObject> authMap, String kafkaClient, LocalMap<String, JsonObject> hostIdMap) {
        this.authMapHolder = authMap;
        this.hostIdMapHolder = hostIdMap;
//        this.kafkaClientHolder = kafkaClient;
    }

    public void eventReadMsg(SelectionKey key) {
        try {
            DatagramChannel channel = (DatagramChannel) key.channel();
            ByteBuffer keyAttach = (ByteBuffer) key.attachment();
            keyAttach.clear();
            InetSocketAddress clientAddr = (InetSocketAddress) channel.receive(keyAttach);
//            final JsonObject dataHost = authMapHolder.get(clientAddr.getHostString());

//            if (dataHost != null) {
                PureMain.logCount++;
//                sendToKafka(keyAttach, dataHost);
//            } else {
//                logger.debug("Datagram from : {} abandoned. (Not In Entry).", clientAddr.getHostString());
//            }
        } catch (IOException e) {
            logger.error("File Writer Event Read Msg Failed : [ " + Utils.getPrintStackTraceToString(e) + " ]");
        }
    }

//    public void sendToKafka(ByteBuffer keyAttach, JsonObject hInfo) {
//        try {
//            keyAttach.flip();
//            if (keyAttach.limit() == 0) {
//                System.out.println("Datagram Body is Null So Discard it : [ " + hInfo.getString("hostid") + " ]");
//                return;
//            }
//            ByteBuffer cloneAttach = ByteBuffer.allocate(keyAttach.limit());
//            keyAttach.get(cloneAttach.array());
//
//            Future<Object> future = Future.future();
//            final String dateTime = ZonedDateTime.now().toLocalDateTime().format(DateTimeFormatter.ofPattern(DEF_FORMAT_YYYYMMDDHHMMSS.get()));
//            final String date = dateTime.substring(0, 8);
//            final String fileName = supplyFileName.call(date);
//            final String hostId = hInfo.getString(SHAREDDATA_KEY_HOSTID);
//            final JsonObject hostInfo = hostIdMapHolder.get(hostId);
//            final String data = new String(cloneAttach.array(), hostInfo.getString(SHAREDDATA_KEY_ENCODE, StandardCharsets.UTF_8.displayName()));
//            final JsonObject parsingMsg = makeParsingMsg.call(hostInfo, fileName, true, dateTime, formattingData.call(dateTime, data));
//
//            kafkaClientHolder.runInThread(future, parsingMsg);
//            future.setHandler(result -> UdpServer.sendingCount++);
//        } catch (Exception e) {
//            logger.error("Event Write Msg Add Failed : [ " + e.getMessage() + " ]");
//            logger.error(Utils.getPrintStackTraceToString(e));
//        }
//    }

//    private Func2<String, String, String> formattingData = (dateTime, data) -> {
//        final Buffer buffer = Buffer.buffer(dateTime).appendString(" ").appendString(data);
//        if (!data.endsWith("\n")) {
//            buffer.appendString("\n");
//        }
//        return buffer.toString();
//    };

}

