package com.innerbus.lcnet.test.UDP.datagram;

import java.nio.channels.SelectionKey;

public interface DatagramIOHandler 
{
	public void eventReadMsg(SelectionKey key);
}
