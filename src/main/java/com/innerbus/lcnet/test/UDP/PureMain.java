package com.innerbus.lcnet.test.UDP;

import com.innerbus.lcnet.common.util.Utils;
import com.innerbus.lcnet.test.UDP.datagram.DatagramSendToThread;
import com.innerbus.lcnet.test.UDP.datagram.UdpListener;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.datagram.DatagramSocket;
import io.vertx.rxjava.core.shareddata.LocalMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PureMain {

    private static final Logger logger = LogManager.getLogger(PureMain.class);
    private static Integer port;
    private static String ip;
    private static LocalMap<String, JsonObject> authMapHolder, hostIdMap;

    private DatagramSocket udpServer;
    private static UdpListener listner;
    public static Long timerId, packetCount = 0L, logCount = 0L, sendingCount = 0L;

    public static void main(String[] args) {
        System.out.println("UDP TEST START===========");
        ip = "0.0.0.0";
//        this.ip = this.config().getString(DEF_BINDIP.get());
        port = 700;
//        this.authMapHolder = getAuthMapFromSharedData
//                .call(this.vertx, PROTOCOL_UDP.getCode(), String.valueOf(this.port));
//        this.kafkaClient = getKafkaClient.call(this.vertx);
//        this.hostIdMap = getHostIdMapFromSharedData.call(this.vertx);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    packetCount += logCount;
                    System.out.printf("UDP Server Received count : %d, Interval count : %d, Send to Kafka : %d %n",
                            packetCount, logCount, sendingCount);
                    logCount = 0L;
                }
            }
        });
        thread.start();

//        Thread thread = new Thread(() -> {
            System.out.println("UdpListener ...");
            try {
                listner = new UdpListener(port, 4096, 1024*1024, 10000,
                        new DatagramSendToThread(authMapHolder, null, hostIdMap));
                listner.UdpServer_Run();
            } catch (Exception e) {
                System.out.println(Utils.getPrintStackTraceToString(e));
            }
//        });
//        thread.start();
    }

//    private Func2<String, String, String> formattingData = (dateTime, data) -> {
//        final Buffer buffer = Buffer.buffer(dateTime).appendString(" ").appendString(data);
//        if (!data.endsWith("\n")) {
//            buffer.appendString("\n");
//        }
//        return buffer.toString();
//    };

//    private Function<DatagramPacket, Handler<Future<Object>>> datagramHandlerInBlocking = datagram -> future -> {
//        final String hostIp = datagram.sender().host();
//        final JsonObject dataHost = authMapHolder.get(hostIp);
//        if (dataHost != null) {
//            final String dateTime = ZonedDateTime.now().toLocalDateTime().format(DateTimeFormatter.ofPattern(DEF_FORMAT_YYYYMMDDHHMMSS.get()));
//            final KafkaClient kafkaClient = getKafkaClient.call(this.vertx);
//            final String date = dateTime.substring(0, 8);
//            final String fileName = supplyFileName.call(date);
//            final String hostId = dataHost.getString(SHAREDDATA_KEY_HOSTID);
//            final JsonObject hostInfo = getHostIdMapFromSharedData.call(this.vertx).get(hostId);
//            final String data = datagram.getDelegate().data().toString(hostInfo.getString(SHAREDDATA_KEY_ENCODE));
//            final JsonObject parsingMsg = makeParsingMsg.call(hostInfo, fileName, true, dateTime, formattingData.call(dateTime, data));
//            kafkaClient.runInThread(future, parsingMsg);
//        } else {
//            logger.debug("Datagram from : {} abandoned. (Not In Entry).", hostIp);
//            future.complete();
//        }
//    };

//    public void start() {
//        this.executor = this.vertx.createSharedWorkerExecutor("test", Runtime.getRuntime().availableProcessors() * 2);
//
//        DatagramSocketOptions opts = new DatagramSocketOptions()
//                .setBroadcast(false).setReuseAddress(true).setReusePort(true);
//
//
//        this.udpServer = this.vertx.createDatagramSocket(opts);
//        this.udpServer.rxListen(this.port, this.ip).subscribe(
//                socket -> socket.toObservable().
//                        subscribe(datagramPacket -> {
//                            logCount++;
////                            this.executor.rxExecuteBlocking(datagramHandlerInBlocking.apply(datagramPacket))
////                                    .subscribe(complete -> sendingCount++,
////                                            error -> logger.error("Datagram Sending Error : {}", error.getMessage()));
//                        }));
//
//        logger.info("UDP Server [ {} : {} ] Deployed With RecvBufferSize:{} Clustered:{} ", this.port, this.deploymentID()
//                , opts.getReceiveBufferSize(), this.vertx.isClustered());
//
//        Integer interval = this.config().getInteger("counting.log.interval", 10000);
//        if (interval != 0) {
//            timerId = this.vertx.setPeriodic(interval, id -> {
//                packetCount += logCount;
//                logger.debug("UDP Server Received count : {}, Interval count : {}, Send to Kafka : {} ",
//                        packetCount, logCount, sendingCount);
//                logCount = 0L;
//            });
//        }
//    }
//
//    public void stop() {
//        if (timerId != null) this.vertx.cancelTimer(timerId);
////        this.listner.Stop_UdpServer();
//        this.udpServer.close(_void -> {
//            logger.info("UdpServer Closed : [ {} : {} ]", port, this.deploymentID());
//        });
//    }
}
